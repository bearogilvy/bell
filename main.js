// const Camera = require('pi-camera-connect');
// const GPIO = require('rpi-gpio');
const Axios = require('axios');

const slackEndpoint = 'https://hooks.slack.com/services/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';

Axios({
    url: slackEndpoint,
    method: 'POST',
    data: {
        attachments: [{
            color: '#36a64f',
            pretext:'Bear is at the door!',
            image_url:'https://static.boredpanda.com/blog/wp-content/uploads/2016/02/cute-baby-polar-bear-day-photography-14__880.jpg'
        }],
    },
});